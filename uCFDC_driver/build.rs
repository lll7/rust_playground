extern crate bindgen;

use failure::Error;
use regex::Regex;
use std::{
    fs::File,
    io::{Read, Write},
    path::{Path, PathBuf},
};
use std::env;

fn out_dir() -> PathBuf {
    std::env::var("OUT_DIR").expect("OUT_DIR environment var not set.").into()
}

fn main() {
    // println!("cargo:rustc-link-lib=bz2");

    let bindings = bindgen::Builder::default()
        .header("ucan_fd_protocol_stm32g431.h")
        // .blacklist_type("FDCAN_HandleTypeDef")
        .generate()
        .expect("Unable to generate bindings");

    let binding_file = out_dir().join("bindings.rs");

    bindings
        .write_to_file(&binding_file)
        .expect("Couldn't write bindings!");
          
    derive_serde(&binding_file).expect("Failed to modify derive macros");

}

fn derive_serde(binding_file: &Path) -> Result<(), Error> {
    let mut contents = String::new();
    File::open(binding_file)?.read_to_string(&mut contents)?;

    let new_contents = format!(
        "use serde::{{Serialize, Deserialize}};\n{}",
        Regex::new(r"#\s*\[\s*derive\s*\((?P<d>[^)]+)\)\s*\]\s*pub\s*(?P<s>struct|enum)")?
            .replace_all(&contents, "#[derive($d, Serialize, Deserialize)] pub $s")
    );

    let new_contents2 = format!(
        "use serde_big_array::big_array;\nbig_array! {{ BigArray; }}\n{}",
        Regex::new(r"pub can_data: \[u8; 64usize\],")?
            .replace_all(&new_contents, " #[serde(with = \"BigArray\")]\n pub can_data: [u8; 64usize], ")
    );

    File::create(&binding_file)?.write_all(new_contents2.as_bytes())?;

    Ok(())
}