#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));



#[cfg(test)]
mod tests {
    use super::*;
    
    use serde::{Deserialize, Serialize};
    use serde_json::{Result, Value};
    use bincode;
    #[test]
    fn test_parsing() {
        let TxFrame = UCAN_TxFrameDef  {
            frame_type: UCAN_FRAME_TYPE_UCAN_FD_TX,
            can_data: [0; 64],
            can_tx_header: FDCAN_TxEventFifoTypeDef{
                Identifier: 0x1234,
                IdType: 0x00000000, //should be taken from fdcan.h why not exported ? FDCAN_DATA_FRAME
                TxFrameType: 0x00000000,
                DataLength: 0x000F0000,
                ErrorStateIndicator: 0x00000000,
                BitRateSwitch: 0x00000000,
                FDFormat: 0x00000000,
                TxTimestamp: 0x00000000,
                MessageMarker: 0x00000000,
                EventType: 0x00000000,
                }
        };

        let serialized_tojson = serde_json::to_string(&TxFrame).unwrap();
        println!("JSON \n {} \n",serialized_tojson);

        let serialized_tobytes =  bincode::serialize(&TxFrame).unwrap();
        println!("Byte \n {:?} \n", serialized_tobytes);
    }
}
