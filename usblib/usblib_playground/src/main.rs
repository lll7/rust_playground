extern crate libusb_sys as ffi;
extern crate libc;

use libc::{c_int,c_uchar};

use std::mem;
use std::ptr;
use std::slice;

fn main() {
  let mut context: *mut ::ffi::libusb_context = unsafe { mem::uninitialized() };

  match unsafe { ::ffi::libusb_init(&mut context) } {
    0 => (),
    e => panic!("libusb_init error:")
  };
  
  unsafe {
    let mut dev_handle: *mut ::ffi::libusb_device_handle =   mem::uninitialized() ;
    let mut usb_context: ::ffi::libusb_context = mem::uninitialized() ;
    let mut usb_data: [c_uchar;3] = [1,2,3]; 
    let mut transferred: c_int = 0;
    // dev_handle =  ::ffi::libusb_open_device_with_vid_pid(&mut usb_context, 0x1209, 0x0775);
    dev_handle =  ::ffi::libusb_open_device_with_vid_pid(ptr::null_mut(), 0x1209, 0x0775);
    
    let mut claim_interface = ::ffi::libusb_claim_interface(dev_handle,0);
    let mut bulk_rest = ::ffi::libusb_bulk_transfer(dev_handle, 0x01, usb_data.as_mut_ptr(),usb_data.len() as c_int, &mut transferred as *mut _ as *mut _, 100);
    // print!(transferred);
    ::ffi::libusb_release_interface(dev_handle,0);
    ::ffi::libusb_close(dev_handle);
    ::ffi::libusb_exit(context) ;
  }
}
