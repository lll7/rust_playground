#include ".\libusb.h"
#include <signal.h>

#define EP_DATA (1 | LIBUSB_ENDPOINT_IN)

static struct libusb_device_handle *devh = NULL;
static struct libusb_transfer *img_transfer = NULL;
static int do_exit = 0;

static unsigned char imgbuf[64] = {0, 1, 2, 3, 4};

static int tranfered;
int bulkres;
static int alloc_transfers(void)
{
    bulkres = libusb_bulk_transfer(devh, EP_DATA, imgbuf, sizeof(imgbuf), &tranfered, 15100);
    if(bulkres == 0)
    {
        printf("Transfer result %i \n", bulkres);
        if (tranfered > 0)
        { 
            printf("USB %d> ", tranfered);
            int loop;
            for(loop = 0; loop < tranfered; loop++)
                printf("%02X ", imgbuf[loop]);
            printf("\n");
        }
    }

    return 0;
}

static void sighandler(int signum)
{
    (void)signum;
    printf("do_exit ...\n");

    do_exit = 1;
}

static int find_dpfp_device(void)
{
    devh = libusb_open_device_with_vid_pid(NULL, 0x1209, 0x0775);
    return devh ? 0 : -EIO;
}

static void LIBUSB_CALL cb_img(struct libusb_transfer *transfer)
{
    if (transfer->status != LIBUSB_TRANSFER_COMPLETED)
    {
        printf("img transfer status %d?\n", transfer->status);
        libusb_free_transfer(transfer);
        return;
    }
    do_exit = 2;
    printf("Image callback\n");
}

int main()
{
    int r = 1;
    signal(SIGINT, sighandler);

    r = libusb_init(NULL);
    if (r < 0)
    {
        printf("failed to initialise libusb\n");
        exit(1);
    }

    find_dpfp_device();
    if (r < 0)
    {
        printf("Could not find/open device\n");
        goto out;
    }

    r = libusb_claim_interface(devh, 0);
    if (r < 0)
    {
        fprintf("usb_claim_interface error %d\n", r);
        goto out;
    }
    printf("claimed interface\n");

    alloc_transfers();
   
    printf("shutting down...\n");

out_deinit:
    libusb_free_transfer(img_transfer);
out_release:
    libusb_release_interface(devh, 0);
out:
    libusb_close(devh);
    libusb_exit(NULL);
    return r >= 0 ? r : -r;
}
