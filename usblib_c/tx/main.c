#include ".\libusb.h"

#define EP_DATA (1 | LIBUSB_ENDPOINT_OUT)

static struct libusb_device_handle *devh = NULL;
static struct libusb_transfer *img_transfer = NULL;
static int do_exit = 0;

static unsigned char imgbuf[16] = {0xFF, 1, 2, 3, 4,5,6,7,8,9,10,11,12,13,14,15};

#define DFU_DNLOAD      1
#define DFU_TIMEOUT 5000
#define DFU_INTERFACE 1


static int tranfered;
int bulkres;


int dfuse_download(const unsigned short length,
		   unsigned char *data, unsigned short transaction)
{
	int status;

	status = libusb_control_transfer(devh,
		 /* bmRequestType */	 LIBUSB_ENDPOINT_OUT |
					 LIBUSB_REQUEST_TYPE_CLASS |
					 LIBUSB_RECIPIENT_INTERFACE,
		 /* bRequest      */	 DFU_DNLOAD,
		 /* wValue        */	 transaction,
		 /* wIndex        */	 DFU_INTERFACE,
		 /* Data          */	 data,
		 /* wLength       */	 length,
					 DFU_TIMEOUT);
	if (status < 0) {
		printf("%s: libusb_control_transfer returned %d",
			__FUNCTION__, status);
	}
	return status;
}

typedef struct {
    unsigned char bStatus;
    unsigned int  bwPollTimeout;
    unsigned char bState;
    unsigned char iString;
} t_dfu_status;

t_dfu_status dfu_status; 

#define DFU_STATUS_ERROR_UNKNOWN        0x0e
#define STATE_DFU_ERROR                 0x0a
#define DFU_GETSTATUS   3
#define QUIRK_POLLTIMEOUT  (1<<0)
#define DEFAULT_POLLTIMEOUT  5

int dfu_get_status(t_dfu_status *status )
{
    unsigned char buffer[6];
    int result;

    /* Initialize the status data structure */
    status->bStatus       = DFU_STATUS_ERROR_UNKNOWN;
    status->bwPollTimeout = 0;
    status->bState        = STATE_DFU_ERROR;
    status->iString       = 0;

    result = libusb_control_transfer( devh,
          /* bmRequestType */ LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_RECIPIENT_INTERFACE,
          /* bRequest      */ DFU_GETSTATUS,
          /* wValue        */ 0,
          /* wIndex        */ DFU_INTERFACE,
          /* Data          */ buffer,
          /* wLength       */ 6,
                              DFU_TIMEOUT );

    if( 6 == result ) {
        status->bStatus = buffer[0];
        status->bwPollTimeout = ((0xff & buffer[3]) << 16) |
                                    ((0xff & buffer[2]) << 8)  |
                                    (0xff & buffer[1]);
        status->bState  = buffer[4];
        status->iString = buffer[5];
    }

    return result;
}

static int find_dpfp_device(void)
{
    devh = libusb_open_device_with_vid_pid(NULL, 0x0483, 0xDF11);
    return devh ? 0 : -EIO;
}

static void LIBUSB_CALL cb_img(struct libusb_transfer *transfer)
{
    if (transfer->status != LIBUSB_TRANSFER_COMPLETED)
    {
        printf("img transfer status %d?\n", transfer->status);
        libusb_free_transfer(transfer);
        return;
    }
    do_exit = 2;
    printf("Image callback\n");
}




int main()
{
    int r = 1;

    r = libusb_init(NULL);
    if (r < 0)
    {
        printf("failed to initialise libusb\n");
        exit(1);
    }

    find_dpfp_device();
    if (r < 0)
    {
        printf("Could not find/open device\n");
        goto out;
    }

    r = libusb_claim_interface(devh, 0);
    if (r < 0)
    {
        fprintf("usb_claim_interface error %d\n", r);
        goto out;
    }
    printf("claimed interface\n");

    // bulkres = libusb_bulk_transfer(devh, EP_DATA, imgbuf, sizeof(imgbuf), &tranfered, 100);
    
    bulkres = dfu_get_status(&dfu_status);
    printf("Transfer1 result %i \n", bulkres);

    bulkres = dfuse_download(0,0,0);
    printf("Transfer2 result %i \n", bulkres);

    printf("shutting down...\n");

out_deinit:
    libusb_free_transfer(img_transfer);
out_release:
    libusb_release_interface(devh, 0);
out:
    libusb_close(devh);
    libusb_exit(NULL);
    return r >= 0 ? r : -r;
}
